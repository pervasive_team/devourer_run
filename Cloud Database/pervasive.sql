-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 26, 2015 at 05:50 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pervasive`
--

-- --------------------------------------------------------

--
-- Table structure for table `challenge`
--

CREATE TABLE IF NOT EXISTS `challenge` (
`challenge_id` int(11) NOT NULL,
  `monster_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `challenge`
--

INSERT INTO `challenge` (`challenge_id`, `monster_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 2),
(5, 2),
(6, 2),
(7, 3),
(8, 3),
(9, 3),
(10, 4),
(11, 4),
(12, 4),
(13, 5),
(14, 5),
(15, 5),
(16, 6),
(17, 6),
(18, 6),
(19, 7),
(20, 7),
(21, 7),
(22, 8),
(23, 8),
(24, 8),
(25, 9),
(26, 9),
(27, 9),
(28, 10),
(29, 10),
(30, 10),
(31, 11),
(32, 11),
(33, 11);

-- --------------------------------------------------------

--
-- Table structure for table `friend`
--

CREATE TABLE IF NOT EXISTS `friend` (
`friend_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `follow` int(11) NOT NULL,
  `date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `game_status`
--

CREATE TABLE IF NOT EXISTS `game_status` (
`status_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `difficulity` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `monster`
--

CREATE TABLE IF NOT EXISTS `monster` (
`monster_id` int(11) NOT NULL,
  `monster_name` varchar(30) NOT NULL,
  `distance1` double NOT NULL,
  `speed1` double NOT NULL,
  `distance2` double NOT NULL,
  `speed2` double NOT NULL,
  `distance3` double NOT NULL,
  `speed3` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `monster`
--

INSERT INTO `monster` (`monster_id`, `monster_name`, `distance1`, `speed1`, `distance2`, `speed2`, `distance3`, `speed3`) VALUES
(1, 'Sluggish', 1, 4, 2, 10, 3, 14),
(2, 'Obesity', 1.5, 5, 3, 11, 4.5, 14),
(3, 'Stress', 2, 6, 4, 12, 6, 14),
(4, 'Sleep deprivation', 1, 4, 2, 10, 3, 14),
(5, 'High blood pressure', 2.5, 7, 5, 12.5, 7.5, 14.4),
(6, 'Coronary heart', 3, 8, 6, 13, 9, 14.5),
(7, 'Stroke', 3.5, 9, 7, 13, 10.5, 14.5),
(8, 'Osteoarthritis', 2.5, 7, 5, 12.5, 7.5, 14.3),
(9, 'Senile', 4, 9.5, 8, 13.5, 12, 14.8),
(10, 'Cancer', 4.5, 10, 9, 14, 13.5, 15),
(11, 'Short live', 5, 10, 10, 14, 15, 15);

-- --------------------------------------------------------

--
-- Table structure for table `record`
--

CREATE TABLE IF NOT EXISTS `record` (
`record_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `date_time` datetime NOT NULL,
  `challenge_id` int(11) NOT NULL,
  `duration` varchar(10) NOT NULL,
  `average_speed` double NOT NULL,
  `status` varchar(10) NOT NULL COMMENT 'fail, pass, or unchange',
  `post` text NOT NULL,
  `facebook_share` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`uid` int(11) NOT NULL,
  `unique_id` varchar(23) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `encrypted_password` varchar(80) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `challenge`
--
ALTER TABLE `challenge`
 ADD PRIMARY KEY (`challenge_id`), ADD KEY `monster_id` (`monster_id`);

--
-- Indexes for table `friend`
--
ALTER TABLE `friend`
 ADD PRIMARY KEY (`friend_id`), ADD KEY `uid` (`uid`), ADD KEY `follow` (`follow`);

--
-- Indexes for table `game_status`
--
ALTER TABLE `game_status`
 ADD PRIMARY KEY (`status_id`), ADD KEY `uid` (`uid`);

--
-- Indexes for table `monster`
--
ALTER TABLE `monster`
 ADD PRIMARY KEY (`monster_id`);

--
-- Indexes for table `record`
--
ALTER TABLE `record`
 ADD PRIMARY KEY (`record_id`), ADD KEY `challenge_id` (`challenge_id`), ADD KEY `uid` (`uid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`uid`), ADD UNIQUE KEY `unique_id` (`unique_id`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `challenge`
--
ALTER TABLE `challenge`
MODIFY `challenge_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `friend`
--
ALTER TABLE `friend`
MODIFY `friend_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `game_status`
--
ALTER TABLE `game_status`
MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `monster`
--
ALTER TABLE `monster`
MODIFY `monster_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `record`
--
ALTER TABLE `record`
MODIFY `record_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `challenge`
--
ALTER TABLE `challenge`
ADD CONSTRAINT `challenge_ibfk_1` FOREIGN KEY (`monster_id`) REFERENCES `monster` (`monster_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `friend`
--
ALTER TABLE `friend`
ADD CONSTRAINT `friend_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `friend_ibfk_2` FOREIGN KEY (`follow`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `game_status`
--
ALTER TABLE `game_status`
ADD CONSTRAINT `game_status_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `record`
--
ALTER TABLE `record`
ADD CONSTRAINT `record_ibfk_1` FOREIGN KEY (`challenge_id`) REFERENCES `challenge` (`challenge_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `record_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
