package com.example.devourer_run;

import android.app.ActionBar.LayoutParams;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class QuarantineFragment extends Fragment { 
	
	public static QuarantineFragment newInstance() {
		return new QuarantineFragment();
	}
  
    @Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  
            Bundle savedInstanceState) {  
    	View rootView = inflater.inflate(R.layout.quarantine, container, false);
        
         return rootView;  
    }  
  
}  