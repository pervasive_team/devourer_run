package com.example.devourer_run;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
                            
 
@SuppressLint("SimpleDateFormat")
public class AlarmService extends IntentService 
{
      
   private static final int NOTIFICATION_ID = 1;
   private static final String TAG = "Devourer Alarm";
   private NotificationManager notificationManager;
   private PendingIntent pendingIntent;
   SQLiteDatabase db;
   int challenge_id;
   String last_date, monster, status, msg;
   Calendar cal1, cal2, cal3, cal;
   long diff, diff2;
   SimpleDateFormat sdf;
   
 
   public AlarmService() {
	   super("AlarmService");
   }
   
   
   @Override
   public int onStartCommand(Intent intent, int flags, int startId) {
       return super.onStartCommand(intent,flags,startId);
   }
   
   @Override
   protected void onHandleIntent(Intent intent) {
	   db=openOrCreateDatabase("Devourer", Context.MODE_PRIVATE, null);
	   
	   Log.d("Database", "Start");
		Cursor c=db.rawQuery("SELECT * FROM record ORDER BY record_id DESC", null);
		if(c.moveToFirst())
		{
			Log.d("Database", "move to first");
			last_date = c.getString(1);
			Log.d("Database string 1", c.getString(1));
			challenge_id = c.getInt(2);
			status = c.getString(5);
			Log.d("Database string 5", c.getString(5));
			
			cal1 = Calendar.getInstance();
			cal2 = Calendar.getInstance();
			cal3 = Calendar.getInstance();
			
			sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
			try {
				cal2.setTime(sdf.parse(last_date));
				cal3.setTime(sdf.parse(last_date));
		    } catch (java.text.ParseException e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		    }
			cal2.add(Calendar.DAY_OF_MONTH, 1);
			cal3.add(Calendar.DAY_OF_MONTH, 2);
			
			Date date = cal1.getTime();
			Date date_1 = cal2.getTime();
			Date date_2 = cal3.getTime();
			
			diff = date_1.getTime() - date.getTime();
			diff2 = date_2.getTime() - date.getTime();
				
			if (status.equals("-")  || status.equals("not_doing")){
				Log.d("status", "--direct");
				cal = cal2;
				status = "direct";
				msg = "Don't forget guys, this time to run!";
			}else{
				Log.d("status", "--change");
				if (diff>0){
					status = "rest";
					msg = "Get more sleep tonight!";
				}else if(diff2>0){
					cal = cal3;
					status = "challenge";
					msg = "Don't forget guys, this time to run!";
				}
			}
			if(status == "direct"){
				if (diff < 0){
					Log.d("Database", "Write not doing for direct");
					final String date_time = cal.getTime().toString();
			    	db.execSQL("INSERT INTO record(record_id, date_time, challenge_id, duration, " +
			    	  	"average_speed, status, post, facebook_share) " +
						"VALUES(null,'"+date_time+"',"+challenge_id+",'-','-', 'not_doing','I not doing the challenge from "+monster+"','-');");
				}
			}else if(status == "challenge"){
				if (diff2 < 0){
					Log.d("Database", "Write not doing for challenge");
					final String date_time = cal.getTime().toString();
			    	db.execSQL("INSERT INTO record(record_id, date_time, challenge_id, duration, " +
			    	  	"average_speed, status, post, facebook_share) " +
						"VALUES(null,'"+date_time+"',"+challenge_id+",'-','-', 'not_doing','I not doing the challenge from "+monster+"','-');");
				}
			}
		}
	   
	   Log.i(TAG,"Alarm Service has started.");
       Context context = this.getApplicationContext();
       notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent mIntent = new Intent(this, MainActivity.class);
        Bundle bundle = new Bundle(); 
        bundle.putString("test", "test");
        mIntent.putExtras(bundle);
		pendingIntent = PendingIntent.getActivity(context, 0, mIntent, PendingIntent.FLAG_UPDATE_CURRENT);     
		
		Resources res = this.getResources();
		NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

		builder.setContentIntent(pendingIntent)
            .setSmallIcon(R.drawable.ic_launcher)
            .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.ic_launcher))
            .setTicker("Devourer! Run")
            .setAutoCancel(true)
            .setContentTitle("Devourer! Run")
            .setContentText(msg);

		notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
		notificationManager.notify(NOTIFICATION_ID, builder.build());
		Log.i(TAG,"Notifications sent.");
    }
}
