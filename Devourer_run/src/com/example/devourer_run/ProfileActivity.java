package com.example.devourer_run;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

@SuppressLint("SimpleDateFormat")
public class ProfileActivity extends Activity {
	SQLiteDatabase db;
	ListView lv;
	private TextView UserName, level, UserDistance;
    String playerName = "Faisal Fahmi";
    int playerLevel = 1;
    int playerDistance = 20;
    SessionManager session;
    ArrayList<Map<String,Object>> mylist= new ArrayList<Map<String,Object>>();
	
	ArrayList<Map<String,Object>> mData= new ArrayList<Map<String,Object>>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);
		setTitle("Devourer!Run");  
		setTitleColor(Color.WHITE);
		
		UserName = (TextView) findViewById(R.id.UserName);
    	level = (TextView) findViewById(R.id.level);
    	UserDistance = (TextView) findViewById(R.id.UserDistance);
    	
        session = new SessionManager(ProfileActivity.this);
        
        session.checkLogin();
        
        HashMap<String, String> user = session.getUserDetails();
        
        playerName = user.get(SessionManager.KEY_NAME);
        
	    UserName.setText(playerName);
	    level.setText(""+playerLevel);
        UserDistance.setText(""+playerDistance);
		
        db=openOrCreateDatabase("Devourer", Context.MODE_PRIVATE, null);
        
      //Getting a cursor to fetch data from the database
        Cursor c=db.rawQuery("SELECT m.monster_name, r.date_time, r.post " +
        		"FROM monster m, record r, challenge c " +
        		"WHERE c.monster_id = m.monster_id AND r.challenge_id = c.challenge_id " +
        		"ORDER BY r.record_id DESC", null);
                
        Log.d("SQLite", "Cursor reference obtained...");
        c.moveToFirst();
        Log.d("SQLite", "Cursor Moved to First Number....");
        if(c!=null){
//If there are contents in the database, then c!=null, so using do-while loop access data // in database
          do{
        	  Map<String,Object> item = new HashMap<String,Object>(); 
        	  String nameMonster=c.getString(c.getColumnIndex("monster_name"));
              String post=c.getString(c.getColumnIndex("post"));
              String date = c.getString(c.getColumnIndex("date_time"));
              
	  	      item.put("monster", nameMonster);  
	  	      item.put("profile", post);  
	  	      item.put("day", date);  
	  	      mData.add(item);
        	                
              c.moveToNext();
          }while(!c.isAfterLast());
        }
		
		lv = (ListView) findViewById(R.id.lv);/*定义一个动态数组*/          
	    
	    SimpleAdapter adapter = new SimpleAdapter(this,mData,R.layout.profile_listview_item,  
	        new String[]{"monster","profile","day"},new int[]{R.id.DefeatMonster,R.id.DefeatProfile,R.id.DayAgo});  
	        
		lv.setAdapter(adapter);//为ListView绑定适配器 
		
        
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}



}
