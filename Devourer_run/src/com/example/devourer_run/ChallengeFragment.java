package com.example.devourer_run;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.GpsStatus.Listener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;

@SuppressLint({ "SimpleDateFormat", "Wakelock" })
public class ChallengeFragment extends Fragment{  
	private TextView txt_status, txt_timer, MonsterName, ChallengeDistance, ChallengeSpeed;
	private Button btn_start;
	MyCount timerCount;
	private Switch switch_gps;
	LocationManager lm;
	private ProgressDialog pDialog;
	SQLiteDatabase db;
	private ImageView imgMonster;
	SimpleDateFormat sdf;
	
	boolean gps = false;
	boolean gps_enabled=false;
	String last_date, monster, status;
	Calendar cal1, cal2, cal3;
	long diff, diff2;
	int level;

	public static ChallengeFragment newInstance() {
		return new ChallengeFragment();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		db=getActivity().openOrCreateDatabase("Devourer", Context.MODE_PRIVATE, null);		
		
		
	}

	@Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  
            Bundle savedInstanceState) {  
    	View rootView = inflater.inflate(R.layout.challenge, container, false);
//    	btnPress = (Button)this.getActivity().findViewById(R.id.buttonPress);
//    	btnPress.setOnClickListener(new View.OnClickListener() {
//    		@Override
//    		public void onClick(View v) {
//    			
//    		}
//    		});
    	txt_status = (TextView) rootView.findViewById(R.id.txt_status);
		txt_timer = (TextView) rootView.findViewById(R.id.txt_timer);
		MonsterName = (TextView) rootView.findViewById(R.id.MonsterName);
		ChallengeDistance = (TextView) rootView.findViewById(R.id.ChallengeDistance);
		ChallengeSpeed = (TextView) rootView.findViewById(R.id.ChallengeSpeed);
		btn_start = (Button) rootView.findViewById(R.id.btn_start);
		switch_gps = (Switch) rootView.findViewById(R.id.switch_gps);
		imgMonster = (ImageView) rootView.findViewById(R.id.imageView1);
		
		Log.d("Database", "Start");
		Cursor c=db.rawQuery("SELECT * FROM record ORDER BY record_id DESC", null);
		if(c.moveToFirst())
		{
			Log.d("Database", "move to first");
			last_date = c.getString(1);
			Log.d("Database string 1", c.getString(1));
			status = c.getString(5);
			Log.d("Database string 5", c.getString(5));
			
			cal1 = Calendar.getInstance();
			cal2 = Calendar.getInstance();
			cal3 = Calendar.getInstance();
			sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
			try {
				cal2.setTime(sdf.parse(last_date));
				cal3.setTime(sdf.parse(last_date));
		    } catch (java.text.ParseException e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		    }
			cal2.add(Calendar.DAY_OF_MONTH, 1);
			cal3.add(Calendar.DAY_OF_MONTH, 2);
			
			Date date = cal1.getTime();
			Date date_1 = cal2.getTime();
			Date date_2 = cal3.getTime();
			
			diff = date_1.getTime() - date.getTime();
			diff2 = date_2.getTime() - date.getTime();
			if (status.equals("-")  || status.equals("not_doing")){
				Log.d("status", " direct");
				status = "direct";
		  		txt_status.setText("Time Remaining :");
				btn_start.setVisibility(View.VISIBLE);
			}else{ //pass or failed
				Log.d("status", "change");
				if (diff>0){
					status = "rest";
					txt_status.setText("Next challenge :");
					btn_start.setVisibility(View.GONE);
				}else if(diff2>0){
					status = "challenge";
					txt_status.setText("Time Remaining :");
					btn_start.setVisibility(View.VISIBLE);
				}
			}
			
			Log.d("last_date", last_date);
			int state = c.getInt(2);
			if (state == 0){
				state = 1;
			}
			Log.d("Database", "start 2");
			c=db.rawQuery("SELECT * FROM challenge c, monster m " +
					"WHERE c.monster_id = m.monster_id AND c.challenge_id = "+state, null);
			Log.d("Database", "end 2");
			if(c.moveToFirst())
			{
				Log.d("Database", "move to first 2");
				MonsterName.setText(c.getString(3) + " Monster");
				level = c.getInt(1);
				monster = c.getString(3);
				ChallengeDistance.setText(c.getDouble(4) + " km");
				ChallengeSpeed.setText(c.getDouble(5) + " kmh");
				if(level == 1){
					imgMonster.setImageResource(R.drawable.challengebackground1);
				}else if(level == 2){
					imgMonster.setImageResource(R.drawable.challengebackground2);
				}else if(level == 3){
					imgMonster.setImageResource(R.drawable.challengebackground3);
				}
				/*
				else if(level == 4){
					imgMonster.setImageResource(R.drawable.challengebackground4);
				}else if(level == 5){
					imgMonster.setImageResource(R.drawable.challengebackground5);
				}else if(level == 6){
					imgMonster.setImageResource(R.drawable.challengebackground6);
				}else if(level == 7){
					imgMonster.setImageResource(R.drawable.challengebackground7);
				}else if(level == 8){
					imgMonster.setImageResource(R.drawable.challengebackground8);
				}else if(level == 9){
					imgMonster.setImageResource(R.drawable.challengebackground9);
				}else if(level == 10){
					imgMonster.setImageResource(R.drawable.challengebackground10);
				}else if(level == 11){
					imgMonster.setImageResource(R.drawable.challengebackground11);
				}
				*/				
			}
		}
		
    	start_timer();
    	
		btn_start.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(gps == true){
					if(!isGpsEnable(getActivity())){					
						Toast.makeText(getActivity(),"GPS not enable", Toast.LENGTH_LONG).show();
					}
				}else{
					//simulation start
					timerCount.cancel();
					Intent intent0 = new Intent(getActivity()
	                        .getApplicationContext(), RunningActivity.class);
					intent0.putExtra("gps_status", gps);
	                startActivity(intent0);
				}
			}
			
			private boolean isGpsEnable(Context context) {
				if(lm==null)
		            lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
				
				try{gps_enabled=lm.isProviderEnabled(LocationManager.GPS_PROVIDER);}catch(Exception ex){}
				
				if(gps_enabled){
					Log.d("process", "GPS Enable");
					if(gps == true){
						//using real measure
						Log.d("process", "GPS Enable true");
						timerCount.cancel();
						lm.addGpsStatusListener(mGPSStatusListener);
						lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListenerGps);
					}
					return true;
				}else{
					return false;
				}
	       	}
			
			
		});
		
		
		
		switch_gps.setOnCheckedChangeListener(new OnCheckedChangeListener() {			 
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
			    boolean isChecked) {			 
				    if(isChecked){
				    	gps = true;
				    }else{
				    	gps = false;
				    }			 
				}
		});
		
        return rootView;
    }
        
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		timerCount.cancel();
	}

	LocationListener locationListenerGps = new LocationListener() {
        
		@Override
		public void onLocationChanged(Location location) {}
		public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extra) {}
    };

	public class MyCount extends CountDownTimer {
	      public MyCount(long millisInFuture, long countDownInterval) {
	        super(millisInFuture, countDownInterval);
	      }

	      @Override
	      public void onFinish() {
	    	  txt_timer.setText("00:00:00");
	    	  Calendar cal = Calendar.getInstance();
			  final String date_time = cal.getTime().toString();
	    	  
			  if(status.equals("challenge") || status.equals("direct")){
	    	  db.execSQL("INSERT INTO record(record_id, date_time, challenge_id, duration, " +
	    	  		"average_speed, status, post, facebook_share) " +
					"VALUES(null,'"+date_time+"',0,'-','-', 'not_doing','I not doing the challenge from "+monster+"','-');");
			  }
	      }

	      @Override
	      public void onTick(long millisUntilFinished) {
	    	  	int secs = (int) (millisUntilFinished / 1000);
				int mins = secs / 60;
				int hours = mins / 60;
				secs = secs % 60;
				mins = mins % 60;
				txt_timer.setText(String.format("%02d", hours) +":" + String.format("%02d", mins) + ":"
							+ String.format("%02d", secs));
	      }   
	} 
	
	
	public void start_timer(){
		long time = 0;
		if (status == "direct"){
			Log.d("time", "direct");
			time = diff;
		} else if(status == "rest"){
			Log.d("time", "rest");
			time = diff;
		}else if(status == "challenge"){
			Log.d("time", "challenge");
			time = diff2;
		}
		timerCount = new MyCount(time, 1000);
	    timerCount.start();
	}
	
	Listener mGPSStatusListener = new GpsStatus.Listener(){    
        public void onGpsStatusChanged(int event) 
        {       
        	Log.d("process", "enter listener");
        	switch(event) 
            {
                case GpsStatus.GPS_EVENT_STARTED:
                	pDialog = ProgressDialog.show(getActivity(), null, "Searching GPS", false, false);
                    break;
                case GpsStatus.GPS_EVENT_STOPPED:
                	/*
                     * GPS_EVENT_STOPPED Event is called when GPS stop           
                     */
                    break;
                case GpsStatus.GPS_EVENT_FIRST_FIX:
                    /*
                     * GPS_EVENT_FIRST_FIX Event is called when GPS is locked            
                     */
                	pDialog.dismiss();
                	
                	Intent intent0 = new Intent(getActivity()
                            .getApplicationContext(), RunningActivity.class);
    				intent0.putExtra("gps_status", gps);
                    startActivity(intent0);
                    
                    lm.removeGpsStatusListener(mGPSStatusListener);
                    break;
                case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                	//System.out.println("TAG - GPS_EVENT_SATELLITE_STATUS");
                    break;                  
           }
       }
    };
}