package com.example.devourer_run;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AlarmReceiver extends BroadcastReceiver {
	Intent intent;
	PendingIntent pendingIntent;
	NotificationManager notificationManager;

	@Override
	public void onReceive(Context context, Intent intent) {
		 Intent service1 = new Intent(context, AlarmService.class);
	     context.startService(service1);
	}
}
