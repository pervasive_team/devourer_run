package com.example.devourer_run;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.ActionBar.Tab;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.GpsStatus.Listener;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class RunningActivity extends Activity {
	
	private TextView duration, YourDistance, Monster1Distance, Monster2Distance, AverageSpeed, RemainingDistance, playerSpeed;
	PowerManager powerManager;
	private SeekBar seekBar;
	private Handler customHandler = new Handler();
	private AudioManager myAudioManager;
	private Button giveup_btn;
	private MediaPlayer information, too_fast, too_slow, win, finish_50, finish_15, failed;
	LocationManager lm;
	private ImageView ImgGps, volumedown, volumeup;
	private ProgressDialog pDialog;
	SQLiteDatabase db;
	
	double plat,plon,clat,clon,dis;
	private long startTime = 0L;
	long timeInMilliseconds = 0L;
	long timeSwapBuff = 0L;
	long updatedTime = 0L;
	double info = 25; //meters
	boolean to_50 = true, to_15 = true;
	double warning = 10;  //second
	boolean warn = false;
	boolean gps = false;
	boolean end = false, success= false;
	double sim_distance = 0;
	String provider;
	int start = 0;
	double monster_distance1 = 0;
	double monster_distance2 = 0;
	JSONParser jsonParser = new JSONParser();
	private static String url = "http://140.113.210.22/pervasive/api/record.php";
	int flag=0;
	private String message;
	
	int id_challenge;
	int uid =3;
	String monster = "sluggish";
	double speed1=1.11;
	double speed2=1.39;
	double finish = 200; //meters
	String finish_duration, average_speed, msg, facebook_share, status;
	double avrg_speed;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_running);
		setTitle("Devourer!Run");  
		setTitleColor(Color.WHITE);
		
		giveup_btn= (Button)findViewById(R.id.giveup);
		duration = (TextView) findViewById(R.id.duration);
		YourDistance = (TextView) findViewById(R.id.YourDistance);
		Monster1Distance = (TextView) findViewById(R.id.Monster1Distance);
		Monster2Distance = (TextView) findViewById(R.id.Monster2Distance);
		AverageSpeed = (TextView) findViewById(R.id.AverageSpeed);
		RemainingDistance = (TextView) findViewById(R.id.RemainingDistance);
		ImgGps = (ImageView) findViewById(R.id.ImgGps);
		volumedown = (ImageView) findViewById(R.id.volumedown);
		volumeup = (ImageView) findViewById(R.id.volumeup);
		seekBar = (SeekBar) findViewById(R.id.seekBar1);
		information = MediaPlayer.create(this, R.raw.information);
		too_fast = MediaPlayer.create(this, R.raw.too_fast);
		too_slow = MediaPlayer.create(this, R.raw.too_slow);
		win = MediaPlayer.create(this, R.raw.finish);
		finish_50 = MediaPlayer.create(this, R.raw.finish_50);
		finish_15 = MediaPlayer.create(this, R.raw.finish_15);
		failed = MediaPlayer.create(this, R.raw.failed);
		playerSpeed = (TextView) findViewById(R.id.playerSpeed);
		
		db=openOrCreateDatabase("Devourer", Context.MODE_PRIVATE, null);
		
		Log.d("Database running", "start 1");
		Cursor c1=db.rawQuery("SELECT * FROM record ORDER BY record_id DESC", null);
		if(c1.moveToFirst())
		{
			Log.d("Database", "move to first 1");
			int state = c1.getInt(2);
			if (state == 0){
				state = 1;
			}
			Log.d("Database running", "start 2");
			int nextLevel = 0;
			Cursor c2=db.rawQuery("SELECT * FROM challenge c, monster m " +
					"WHERE c.monster_id = m.monster_id AND c.challenge_id = "+state, null);
			Log.d("Database", "end 2");
			if(c2.moveToFirst())
			{
				Log.d("Database", "move to first 2");
				speed1 = c2.getDouble(5)/3.6;
				id_challenge = c2.getInt(0);
				monster = c2.getString(3);
				finish = c2.getDouble(4) * 1000;
				nextLevel = c2.getInt(1) + 1;
			}
			Log.d("Database running", "start 3");
			Cursor c3=db.rawQuery("SELECT * FROM challenge c, monster m " +
					"WHERE c.monster_id = m.monster_id AND c.monster_id = " + nextLevel, null);
			Log.d("Database", "end 3");
			if(c3.moveToFirst())
			{
				Log.d("Database", "move to first 3");
				speed2 = c3.getDouble(5)/3.6;
			}
		}
		
		if (getIntent() != null) {
			lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			gps = getIntent().getBooleanExtra("gps_status",false);
		    if (gps == true){
		    	lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListenerGps);
			    seekBar.setVisibility(View.GONE);
		    	playerSpeed.setVisibility(View.GONE);
		    }else{
		    	seekBar.setVisibility(View.VISIBLE);
		    	playerSpeed.setVisibility(View.VISIBLE);
		    }
		}
		
		giveup_btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder=new AlertDialog.Builder(RunningActivity.this);
				builder.setTitle("Confirm");
				builder.setMessage("Are you sure want to give up?");
				builder.setCancelable(false);
				builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						Calendar c = Calendar.getInstance();
						final String date_time = c.getTime().toString();
						facebook_share = "false";
						status = "failed";
						msg = "I was give up to defeat " + monster + " monster with run " + String.format("%.2f", finish/1000) + " km and speed " + 
								String.format("%.2f", avrg_speed) + " kmh";
						db.execSQL("INSERT INTO record(record_id, date_time, challenge_id, duration, average_speed, status, post, facebook_share) " +
								"VALUES(null,'"+date_time+"',"+id_challenge+",'"+finish_duration+"','"+average_speed+"', 'failed','"+msg+"','false');");
						
						if(isOnline(RunningActivity.this))
						{					
							new sendServer().execute();
						}else{
							Toast.makeText(RunningActivity.this,"No network connection, cannot send data to server", Toast.LENGTH_LONG).show();
							kill_activity();
							return;
						}
					}
				});
				
				builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						arg0.cancel();
					}
				});
				
				AlertDialog alertDialog=builder.create();
				alertDialog.show();	
			}
		});
		
		volumeup.setOnClickListener(new View.OnClickListener(){
	  		
	  		@Override
	  		public void onClick(View view) {
	  			// increase the volume and show the ui
	  			myAudioManager.adjustVolume(AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
	  		}
	  	});
		
		volumedown.setOnClickListener(new View.OnClickListener(){
	        @Override
	        public void onClick(View view) {
	            // decrease the volume and show the ui
	            myAudioManager.adjustVolume(AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
	        }
		});
		
		playerSpeed.setText(""+seekBar.getProgress());
		  
		seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			int progress = 0;
			  @Override
			  public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
				  progress = progresValue;
			  }
			
			  @Override
			  public void onStartTrackingTouch(SeekBar seekBar) {}
			
			  @Override
			  public void onStopTrackingTouch(SeekBar seekBar) {
				  playerSpeed.setText(""+progress);				  
			  }
		});
		
		startTime = SystemClock.uptimeMillis();
		customHandler.postDelayed(updateTimerThread, 1000);
		
		myAudioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
	}
	
	private boolean isOnline(Context mContext) {
		ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting())
	   	  {
			return true;
     		}
		    return false;
    }
	
	private Runnable updateTimerThread = new Runnable() {
		@SuppressLint("Wakelock")
		public void run() {
			powerManager = (PowerManager) getSystemService(POWER_SERVICE);
			WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
			        "MyWakelockTag");
			wakeLock.acquire();
			
			timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
			
			updatedTime = timeSwapBuff + timeInMilliseconds;

			int secs = (int) (updatedTime / 1000);
			int mins = secs / 60;
			int hours = mins / 60;
			secs = secs % 60;
			mins = mins % 60;
			
			double playerSpeed = (double)seekBar.getProgress()/100;
			
			if(secs>0){
				monster_distance1 += speed1;
				monster_distance2 += speed2;
				if (gps == true){
					sim_distance = dis;
				}else{
					sim_distance += playerSpeed;
				}
			}
			
			
			double remain = (double)finish - sim_distance;
			
			
			if(sim_distance>=info && warn == false){
				information.start();
				info += 25;
			}else if(sim_distance>=info && warn == true){
				info += 25;
			}
			if(remain < 50){
				if(remain <= 50 && to_50 == true){
					finish_50.start();
					to_50 = false;
				}
				if(remain <= 15 && to_15 == true){
					finish_15.start();
					to_15 = false;
				}
			}
			
			
			
			double diff_distance1 = sim_distance-monster_distance1;
			double diff_distance2 = monster_distance2-sim_distance;
			if(mins > 0){
				if (secs == warning){
					warn = false;
					if (diff_distance1 < 5){
						too_slow.start();
						warn = true;
					}
					
					if (diff_distance2 < 5){
						too_fast.start();
						warn = true;
					}
					
					if(warning == 50){
						warning = 0;
					}else{
						warning +=10;
					}
				}
			}
			
			if(sim_distance>=finish && (diff_distance1 > 0 && diff_distance2 > 0)){
				win.start();
				success = true;
				end = true;
			}else if(sim_distance>=finish && (diff_distance1 < 0 || diff_distance2 < 0)){
				failed.start();
				end = true;
			}
			
			double av_speed =  sim_distance / ((hours * 3600)+(mins * 60)+ secs);
			final String dur= String.format("%02d", hours) +":" + String.format("%02d", mins) + ":" + String.format("%02d", secs);
			duration.setText(dur);
			YourDistance.setText(String.format("%.2f", sim_distance/1000));
			Monster1Distance.setText(String.format("%.2f", monster_distance1/1000));
			Monster2Distance.setText(String.format("%.2f", monster_distance2/1000));
			final String speed = "" + String.format("%.2f", av_speed*3.6) + "";
			AverageSpeed.setText(speed);
			if(remain > 0){
				RemainingDistance.setText(String.format("%.2f", remain/1000));
			}else{
				RemainingDistance.setText("0.00");
			}
			
			Calendar c = Calendar.getInstance();
			//component to save in database
			final String date_time = c.getTime().toString();
			finish_duration = dur;
			average_speed = speed;
			avrg_speed = av_speed*3.6;
			
			if(end == true && success == true){
				//component to save in database
				msg = "I was defeated " + monster + " monster with run " + String.format("%.2f", finish/1000) + " km and speed " + 
						String.format("%.2f", av_speed*3.6) + " kmh";
				
				AlertDialog.Builder builder=new AlertDialog.Builder(RunningActivity.this);
				builder.setTitle("Share to Facebook");
				builder.setMessage(msg);
				builder.setCancelable(false);
				builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						//Save to local database
						Log.d("start share facebook", "yes");
						facebook_share = "true";
						status = "pass";
						db.execSQL("INSERT INTO record(record_id, date_time, challenge_id, duration, average_speed, status, post, facebook_share) " +
								"VALUES(null,'"+date_time+"',"+id_challenge+",'"+finish_duration+"','"+average_speed+"', 'pass','"+msg+"','true');");
						
						
						/*
						 * SHARE TO FACEBOOK HERE
						 */
						
						if(isOnline(RunningActivity.this))
						{					
							new sendServer().execute();
						}else{
							Toast.makeText(RunningActivity.this,"No network connection, cannot send data to server", Toast.LENGTH_LONG).show();
							finish();
							return;
						}
						
						win.stop();
						Log.d("end share facebook", "yes");
					}
				});
				
				builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						//Save to local database
						Log.d("start share facebook", "no");
						facebook_share = "false";
						status = "pass";
						db.execSQL("INSERT INTO record(record_id, date_time, challenge_id, duration, average_speed, status, post, facebook_share) " +
								"VALUES(null,'"+date_time+"',"+id_challenge+",'"+finish_duration+"','"+average_speed+"', 'pass','"+msg+"','false');");

						if(isOnline(RunningActivity.this))
						{					
							new sendServer().execute();
						}else{
							Toast.makeText(RunningActivity.this,"No network connection, cannot send data to server", Toast.LENGTH_LONG).show();
							finish();
							return;
						}
						
						win.stop();
						Log.d("end share facebook", "no");
					}
				});
				AlertDialog alertDialog=builder.create();
				alertDialog.show();
				
				customHandler.removeCallbacks(updateTimerThread);
				wakeLock.release();
				
			}else if(end == true && success == false){
				//component to save in database
				facebook_share = "false";
				status = "failed";
				msg = "I was failed to defeat " + monster + " monster with run " + String.format("%.2f", finish/1000) + " km and speed " + 
						String.format("%.2f", av_speed*3.6) + " kmh";
				
				AlertDialog alertDialog = new AlertDialog.Builder(RunningActivity.this).create();
				alertDialog.setTitle("Information");
				alertDialog.setMessage(msg);
				alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
				    new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog, int which) {
				        	
				        	Log.d("finish start", "failed");
							db.execSQL("INSERT INTO record(record_id, date_time, challenge_id, duration, average_speed, status, post, facebook_share) " +
									"VALUES(null,'"+date_time+"',"+id_challenge+",'"+finish_duration+"','"+average_speed+"', 'failed','"+msg+"','false');");
							Log.d("finish end", "failed");
							dialog.dismiss();
							
							if(isOnline(RunningActivity.this))
							{					
								new sendServer().execute();
							}else{
								Toast.makeText(RunningActivity.this,"No network connection, cannot send data to server", Toast.LENGTH_LONG).show();
								finish();
								return;
							}
							
							win.stop();
				         }
				    });
				alertDialog.show();
				customHandler.removeCallbacks(updateTimerThread);
				wakeLock.release();
			}else if(end == false){
				customHandler.postDelayed(this, 1000);
			}	
		}
	};
	
	LocationListener locationListenerGps = new LocationListener() {
		@Override
		public void onLocationChanged(Location location) {
			if(start == 0){
				plat = location.getLatitude();
				plon = location.getLongitude();
			}
			
			clat=location.getLatitude();
	        clon=location.getLongitude();
	        if(clat!=plat || clon!=plon){
	            dis+=getDistance(plat,plon,clat,clon);
	            plat=clat;
	            plon=clon;
	        }
	        
	        start = 1;
		}
		public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extra) {}
    };
    
    public Listener mGPSStatusListener = new GpsStatus.Listener()
    {    
        public void onGpsStatusChanged(int event) 
        {       
            switch(event) 
            {
                case GpsStatus.GPS_EVENT_STARTED:
                	pDialog = ProgressDialog.show(RunningActivity.this, null, "Searching GPS", false, false);     
                    break;
                case GpsStatus.GPS_EVENT_STOPPED:
                	ImgGps.setImageResource(R.drawable.no_gps);
                    break;
                case GpsStatus.GPS_EVENT_FIRST_FIX:
                    /*
                     * GPS_EVENT_FIRST_FIX Event is called when GPS is locked            
                     */
                	ImgGps.setImageResource(R.drawable.gps);
                	pDialog.dismiss();
                    
                	startTime = SystemClock.uptimeMillis();
        			customHandler.postDelayed(updateTimerThread, 0);
        				
                    lm.removeGpsStatusListener(mGPSStatusListener);                     
                    break;
                case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                		//System.out.println("TAG - GPS_EVENT_SATELLITE_STATUS");
                    break;                  
           }
       }
    };
    
    public double getDistance(double lat1, double lon1, double lat2, double lon2) {
	    double latA = Math.toRadians(lat1);
	    double lonA = Math.toRadians(lon1);
	    double latB = Math.toRadians(lat2);
	    double lonB = Math.toRadians(lon2);
	    double cosAng = (Math.cos(latA) * Math.cos(latB) * Math.cos(lonB-lonA)) +
	                    (Math.sin(latA) * Math.sin(latB));
	    double ang = Math.acos(cosAng);
	    double dist = ang *6371 *1000;
	    dist = Math.round(dist);
	    if (dist > 10){
	    	dist = 0;
	    }
	    return dist;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	void kill_activity()
	{ 
		customHandler.removeCallbacks(updateTimerThread);
		finish();
	}
	
	class sendServer extends AsyncTask<String, String, String> {

		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(RunningActivity.this);
			pDialog.setMessage("Send data to server...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}
		@Override
		protected String doInBackground(String... arg0) {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			
			params.add(new BasicNameValuePair("uid", ""+uid));
			params.add(new BasicNameValuePair("challenge_id", ""+id_challenge));
			params.add(new BasicNameValuePair("duration", finish_duration));
			params.add(new BasicNameValuePair("average_speed", ""+avrg_speed));
			params.add(new BasicNameValuePair("status", status));
			params.add(new BasicNameValuePair("post", msg));
			params.add(new BasicNameValuePair("facebook_share", facebook_share));
			
			JSONObject json = jsonParser.makeHttpRequest(url,"POST", params);
			Log.d("Create Response", json.toString());
			
			try {
				int success = json.getInt("success");
				message = json.getString("message").trim();
				if (success == 1) 
				{
					flag=0;
					kill_activity();
				}
				else
				{
					// failed to store in cloud
					flag=1;
					kill_activity();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;
		}
		protected void onPostExecute(String file_url) {
			pDialog.dismiss();
			if(flag==1)
				Toast.makeText(RunningActivity.this, message, Toast.LENGTH_LONG).show();
		}
		
	  }
}
