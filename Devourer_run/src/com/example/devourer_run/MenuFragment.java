package com.example.devourer_run;



import java.util.Calendar;
import java.util.HashMap;

import com.example.devourer_run.RunningActivity.sendServer;

import android.support.v4.app.Fragment;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MenuFragment extends Fragment {
	private LinearLayout UserProfile,Friend,Statistic,Logout;
	SessionManager session;
	private TextView UserName;
	String playerName = "Faisal Fahmi";
    
	public static MenuFragment newInstance() {
		return new MenuFragment();
	}

	@Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  
            Bundle savedInstanceState) {
//		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//		.detectDiskReads().detectDiskWrites().detectNetwork()
//		.penaltyLog().build());
		session = new SessionManager(getActivity().getApplicationContext());
		 
        session.checkLogin();

    	View rootView = inflater.inflate(R.layout.menu, container, false);
    	
    	HashMap<String, String> user = session.getUserDetails();
        
        playerName = user.get(SessionManager.KEY_NAME);
    	
        UserName = (TextView) rootView.findViewById(R.id.UserName);
    	UserProfile= (LinearLayout) rootView.findViewById(R.id.UserProfile);
    	UserProfile.setOnClickListener(onclicklistener);
    	Friend= (LinearLayout) rootView.findViewById(R.id.Friend);
    	Friend.setOnClickListener(onclicklistener);
    	Statistic= (LinearLayout) rootView.findViewById(R.id.Statistic);
    	Statistic.setOnClickListener(onclicklistener);
    	Logout= (LinearLayout) rootView.findViewById(R.id.Logout);
    	Logout.setOnClickListener(onclicklistener);
    	
    	UserName.setText(playerName);

        return rootView;  
    }
	
	public OnClickListener onclicklistener=new OnClickListener(){

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.UserProfile:
				Intent intent0 = new Intent(getActivity().getApplicationContext(), ProfileActivity.class);
				startActivity(intent0);
				break;
			case R.id.Friend:
				Intent intent1 = new Intent(getActivity().getApplicationContext(), FriendActivity.class);
				startActivity(intent1);
				break;
			case R.id.Statistic:
				Intent intent2 = new Intent(getActivity().getApplicationContext(), StatisticsActivity.class);
				startActivity(intent2);
				break;
			case R.id.Logout:
				
				AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
				builder.setTitle("Confirm");
				builder.setMessage("Are you sure want to logout?");
				builder.setCancelable(false);
				builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						session.logoutUser();
						Intent intent3 = new Intent(getActivity().getApplicationContext(), LoginActivity.class);
						startActivity(intent3);
						getActivity().finish();
					}
				});
				
				builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						arg0.cancel();
					}
				});
				
				AlertDialog alertDialog=builder.create();
				alertDialog.show();	
				
				break;	
			}
		}
		
	};
	
	


}
