/*
 * Copyright (c) 2014, 闈掑矝鍙搁�绉戞妧鏈夐檺鍏徃 All rights reserved.
 * File Name锛歋ectionsPagerAdapter.java
 * Version锛歏1.0
 * Author锛歾haokaiqiang
 * Date锛�014-10-29
 */

package com.example.devourer_run;

import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

public class SectionsPagerAdapter extends FragmentPagerAdapter {

//	private String[] titles = new String[] { "TAB1", "TAB2", "TAB3" };
	
	private int[] icons = new int[] {R.drawable.home_tabicon, R.drawable .quarantine_tabicon, R.drawable.challenge_tabicon,R.drawable.menu_tabicon};

	public SectionsPagerAdapter(FragmentManager fragmentManager) {
		super(fragmentManager);
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {
        case 0:
            // The first section of the app is the most interesting -- it offers
            // a launchpad into the other demonstrations in this example application.
        	
            return HomeFragment.newInstance();
        case 1:
            return QuarantineFragment.newInstance();
            
        case 2:
            return ChallengeFragment.newInstance();
            
        case 3:
            return MenuFragment.newInstance();

        default:
            // The other sections of the app are dummy placeholders.
        	Log.d("XXXX", "In dummy page");
    		return PlaceholderFragment.newInstance();

    }

		
	}

	@Override
	public int getCount() {
		return 4;
	}

	public int getPageIcon(int position) {
		return icons[position];
	}
//	@Override
//	public CharSequence getPageTitle(int position) {
//		return titles[position];
//	}

}
