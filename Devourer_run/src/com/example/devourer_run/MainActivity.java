package com.example.devourer_run;

import java.util.Calendar;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.widget.ProfilePictureView;
import com.facebook.share.widget.ShareDialog;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends FragmentActivity implements ActionBar.TabListener{
	ViewPager mViewPager;
	private ActionBar actionBar;
	SectionsPagerAdapter mSectionsPagerAdapter;
	


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		  setTitle("Devourer!Run");  
		  setTitleColor(Color.WHITE);
//		    ActionBar actionBar = getActionBar();  
//		    actionBar.setDisplayHomeAsUpEnabled(true);  
//		    setOverflowShowingAlways();  
//		    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);  
//		    Tab	tab = actionBar  
//            .newTab()  
//            .setIcon(R.drawable.home_tabicon)  
//		            .setTabListener(  
//                    new TabListener<HomeFragment>(this, "home",  
//                            HomeFragment.class));  
//		    actionBar.addTab(tab);  
//		        
//		    tab = actionBar  
//		            .newTab()  
//		            .setIcon(R.drawable.quarantine_tabicon)  
//		            .setTabListener(  
//		                    new TabListener<QuarantineFragment>(this, "quarantine",  
//		                    		QuarantineFragment.class));  
//		    actionBar.addTab(tab);  
//		    
//		    tab = actionBar  
//		            .newTab()  
//		            .setIcon(R.drawable.challenge_tabicon)  
//		            .setTabListener(  
//		                    new TabListener<ChallengeFragment>(this, "challenge",  
//		                    		ChallengeFragment.class));  
//		    
//		    
//		    actionBar.addTab(tab);
//            tab = actionBar  
//		            .newTab()  
//		            .setIcon(R.drawable.menu_tabicon)  
//		            .setTabListener(  
//		                    new TabListener<MenuFragment>(this, "menu",  
//		                    		MenuFragment.class));  
//		    
//		    
//		    actionBar.addTab(tab);
		    
		    //Sliding_TAB_ActionBar
		    mViewPager = (ViewPager) findViewById(R.id.pager);
		    actionBar = getActionBar();
			// 璁剧疆瀵艰埅妯″紡涓篢ab鏂瑰紡
			actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
			// 璁剧疆鏄剧ず杩斿洖鍓ご
			getActionBar().setDisplayHomeAsUpEnabled(false);
			mSectionsPagerAdapter = new SectionsPagerAdapter(
					getSupportFragmentManager());
			mViewPager.setAdapter(mSectionsPagerAdapter);

			// 鍦ㄤ笉鍚岀殑Fragment涔嬮棿婊戝姩鐨勬椂鍊欙紝淇敼閫変腑鐨則ab锛屾垜浠篃鍙互浣跨敤ActionBar.Tab#select()瀹屾垚锛屽鏋滄垜浠湁Tab鐨勫紩鐢ㄧ殑璇�		
			mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
						@Override
						public void onPageSelected(int position) {
							actionBar.setSelectedNavigationItem(position);
						}
					});

			// 鏍规嵁鐣岄潰鏁伴噺娣诲姞Tab
			for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {

				actionBar.addTab(actionBar.newTab()
						.setIcon(mSectionsPagerAdapter.getPageIcon(i))
						.setTabListener(this));
			}

		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    Intent intent = new Intent(this, AlarmReceiver.class);
			PendingIntent sender = PendingIntent.getBroadcast(this, 0, intent, 0);
			AlarmManager am = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
			long recurring = (2*60*60*1000);  // in milliseconds
			am.setRepeating(AlarmManager.RTC, Calendar.getInstance().getTimeInMillis(), recurring, sender);
			
			
			if(!isOnline(MainActivity.this))
			{					
				Toast.makeText(MainActivity.this,"No network connection", Toast.LENGTH_LONG).show();
				return;	
			}
			
	}
	
	private boolean isOnline(Context mContext) {
		ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting())
	   	  {
			return true;
     		}
		    return false;
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
		
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}
	
	//====//needed for FACEBOOK//============//
	private Button postStatusUpdateButton;
	private Button postPhotoButton;
    private ProfilePictureView profilePictureView;
    private TextView greeting;
 //   private PendingAction pendingAction = PendingAction.NONE;
    private boolean canPresentShareDialog;
    private boolean canPresentShareDialogWithPhotos;
    private CallbackManager callbackManager;
    private ProfileTracker profileTracker;
    private ShareDialog shareDialog;
    
	@Override
    protected void onResume() {
        super.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onResume methods of the primary Activities that an app may be
        // launched into.
        
        //AppEventsLogger.activateApp(this);

        //updateUI();
    }
	
	private void updateUI() {
        boolean enableButtons = AccessToken.getCurrentAccessToken() != null;

        postStatusUpdateButton.setEnabled(enableButtons || canPresentShareDialog);
        postPhotoButton.setEnabled(enableButtons || canPresentShareDialogWithPhotos);

        Profile profile = Profile.getCurrentProfile();
        if (enableButtons && profile != null) {
            profilePictureView.setProfileId(profile.getId());
            greeting.setText(getString(R.string.hello_user, profile.getFirstName()));
        } else {
            profilePictureView.setProfileId(null);
            greeting.setText(null);
        }
    }
}
