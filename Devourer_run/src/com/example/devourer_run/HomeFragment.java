package com.example.devourer_run;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.devourer_run.LoginActivity.loginAccess;

import android.app.ActionBar.LayoutParams;
import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class HomeFragment extends Fragment {  
	SQLiteDatabase db;
	ListView lv;
	ArrayList<Map<String,Object>> mData= new ArrayList<Map<String,Object>>();
	ArrayList<Map<String,Object>> mylist= new ArrayList<Map<String,Object>>();
	private ListView list ;  
    private SimpleAdapter adapter;
    String email, uid;
    private TextView UserName, level, UserDistance;
    String playerName = "Faisal Fahmi";
    int playerLevel = 1;
    int playerDistance = 20;
    SessionManager session;
    private ProgressDialog pDialog;
    JSONParser jsonParser = new JSONParser();
    JSONArrayParser jsonArrParser = new JSONArrayParser();
	private static String url = "http://140.113.210.22/pervasive/api/friend_record.php";
	int flag=0;
	JSONObject json;
	JSONArray jarray;

    public static HomeFragment newInstance() {
		return new HomeFragment();
	}

	
    @Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  
            Bundle savedInstanceState) {  
    	View rootView = inflater.inflate(R.layout.home, container, false);
    	
    	UserName = (TextView) rootView.findViewById(R.id.UserName);
    	level = (TextView) rootView.findViewById(R.id.level);
    	UserDistance = (TextView) rootView.findViewById(R.id.UserDistance);
    	
        session = new SessionManager(getActivity().getApplicationContext());
        
        session.checkLogin();
        
        HashMap<String, String> user = session.getUserDetails();
        
        playerName = user.get(SessionManager.KEY_NAME);
        email = user.get(SessionManager.KEY_EMAIL);
        uid = user.get(SessionManager.KEY_UID);
        
        int uidi=0;
        
        try{
        	uidi = Integer.parseInt(uid);
        }catch(NumberFormatException ex) {
            System.err.println("NumberFormatException in parseInt, "+ ex.getMessage());
        }
        
	    UserName.setText(playerName);
        level.setText(""+playerLevel);
        UserDistance.setText(""+playerDistance);
        
        if(!isOnline(getActivity()))
		{					
			Toast.makeText(getActivity(),"No network connection", Toast.LENGTH_LONG).show();
		}else{
			new readFriendRecord().execute();
		}
        
        
        
        lv = (ListView) rootView.findViewById(R.id.lv);

	    
        return rootView;
    }
    
    private boolean isOnline(Context mContext) {
		ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting())
	   	{
			return true;
     	}
		return false;
    }
    
    class readFriendRecord extends AsyncTask<String, String, String> {

    	protected void onPreExecute() {
    		super.onPreExecute();
    		pDialog = new ProgressDialog(getActivity());
    		pDialog.setMessage("Login...");
    		pDialog.setIndeterminate(false);
    		pDialog.setCancelable(true);
    		pDialog.show();
    	}
    	@Override
    	protected String doInBackground(String... arg0) {
    		List<NameValuePair> params = new ArrayList<NameValuePair>();
    		params.add(new BasicNameValuePair("uid", uid));
    		//json = jsonParser.makeHttpRequest(url,"POST", params);
    		String jsn = jsonArrParser.makeHttpRequest(url,"POST", params);
    		Log.d("Create Response", jsn);
    		
    				
			try{
				JSONObject jsonObj = new JSONObject(jsn);
				JSONArray jsonArray = jsonObj.getJSONArray("data");
      	       	//Loop the Array
				mylist.clear();
		        for(int i=0;i < jsonArray.length();i++){						

		        	Map<String,Object> item = new HashMap<String,Object>();  
		        	JSONObject e = jsonArray.getJSONObject(i);
		        	Log.d("After Response", e.toString());
		        	item.put("friendpicture", R.drawable.default_person);
		        	
			        item.put("friendname", e.getString("name"));  
			        item.put("friendprofile", e.getString("post"));  
			        item.put("friendday", e.getString("date_time"));
		        	mylist.add(item);
				}
		        Log.d("After Response", mylist.toString());
	       }catch(JSONException e)        {
	       	 Log.e("log_tag", "Error parsing data "+e.toString());
	       }
    				
    		
    		return null;
    	}
    	
    	protected void onPostExecute(String file_url) {
    		pDialog.dismiss();
    		
    		SimpleAdapter adapter = new SimpleAdapter(getActivity(),mylist,R.layout.home_listview_item,  
    		        new String[]{"friendpicture","friendname","friendprofile","friendday"},
    		        new int[]{R.id.FriendPicture,R.id.FriendName,R.id.FriendProfile,R.id.FriendDayAgo});  
    		        
    			lv.setAdapter(adapter);
    		
    	}
    	
      }
    

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		
		db=getActivity().openOrCreateDatabase("Devourer", Context.MODE_PRIVATE, null);
		db.execSQL("CREATE TABLE IF NOT EXISTS record(record_id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"date_time VARCHAR," +
				"challenge_id INTEGER," +
				"duration VARCHAR," +
				"average_speed VARCHAR, " +
				"status VARCHAR, " +
				"post VARCHAR," +
				"facebook_share VARCHAR);");
		
		db.execSQL("CREATE TABLE IF NOT EXISTS challenge(challenge_id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"monster_id INTEGER);");
		
		db.execSQL("CREATE TABLE IF NOT EXISTS monster(monster_id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"monster_name VARCHAR," +
				"distance1 DOUBLE," +
				"speed1 DOUBLE," +
				"distance2 DOUBLE," +
				"speed2 DOUBLE," +
				"distance3 DOUBLE," +
				"speed3 DOUBLE);");
		
		Cursor c=db.rawQuery("SELECT * FROM challenge", null);
		if(c.getCount()==0)
		{
			db.execSQL("INSERT INTO challenge(challenge_id, monster_id) " +
					"VALUES(1,1),(2,1),(3,1)," +
					"(4,2),(5,2),(6,2)," +
					"(7,3),(8,3),(9,3)," +
					"(10,4),(11,4),(12,4)," +
					"(13,5),(14,5),(15,5)," +
					"(16,6),(17,6),(18,6)," +
					"(19,7),(20,7),(21,7)," +
					"(22,8),(23,8),(24,8)," +
					"(25,9),(26,9),(27,9)," +
					"(28,10),(29,10),(30,10)," +
					"(31,11),(32,11),(33,11);");
		}
		
		c=db.rawQuery("SELECT * FROM monster", null);
		if(c.getCount()==0)
		{
			db.execSQL("INSERT INTO monster(monster_id, monster_name, distance1, speed1, distance2, speed2, distance3, speed3) " +
					"VALUES(1,'Sluggish',0.2,4,2,10,3,14)," + //"VALUES(1,'Sluggish',1,4,2,10,3,14)," +
					"(2,'Obesity',1.5,5,3,11,4.5,14)," +
					"(3,'Stress',2,6,4,12,6,14)," +
					"(4,'Sleep deprivation',1,4,2,10,3,14)," +
					"(5,'High blood pressure',2.5,7,5,12.5,7.5,14.4)," +
					"(6,'Coronary heart',3,8,6,13,9,14.5)," +
					"(7,'Stroke',3.5,9,7,13,10.5,14.5)," +
					"(8,'Osteoarthritis',2.5,7,5,12.5,7.5,14.3)," +
					"(9,'Senile',4,9.5,8,13.5,12,14.8)," +
					"(10,'Cancer',4.5,10,9,14,13.5,15)," +
					"(11,'Short live',5,10,10,14,15,15);");
		}
		
		c=db.rawQuery("SELECT * FROM record", null);
		if(c.getCount()==0)
		{	
			Calendar cal = Calendar.getInstance();
			final String date_time = cal.getTime().toString();
			
			db.execSQL("INSERT INTO record(record_id, date_time, challenge_id, duration, average_speed, status, post, facebook_share) " +
					"VALUES(null,'"+date_time+"',0,'-','-', '-','I was installed Devourer!Run','-');");
		}
	} 
}  